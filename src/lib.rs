#![allow(non_camel_case_types)]
extern crate libc;
pub const DB_VERSION_MAJOR: u32 = 4;
pub const DB_VERSION_MINOR: u32 = 3;
pub const DB_VERSION_PATCH: u32 = 29;
pub const DB_VERSION_STRING: &'static str =
    r#"Sleepycat Software: Berkeley DB 4.3.29: (September 21, 2005)"#;
type u_int8_t = u8;
type int16_t = i16;
type u_int16_t = u16;
type int32_t = i32;
pub type u_int32_t = u32;
pub type int64_t = i64;
type u_int64_t = u64;
pub type void = libc::c_void;
pub type int = libc::c_int;
pub type size_t = libc::size_t;
type time_t = libc::time_t;
type FILE = libc::FILE;
type double = libc::c_double;
type long = libc::c_long;
pub type off_t = libc::off_t;
pub type char = libc::c_schar;
type u_char = libc::c_uchar;
type u_short = libc::c_ushort;
type u_int = libc::c_uint;
pub type u_long = libc::c_ulong;
#[cfg(target_arch="x86_64")]
pub type ssize_t = int64_t;
#[cfg(not(target_arch="x86_64"))]
pub type ssize_t = int32_t;
type uintmax_t = u_int64_t;
#[cfg(target_arch="x86_64")]
type uintptr_t = u_int64_t;
#[cfg(not(target_arch="x86_64"))]
type uintptr_t = u_int32_t;
type db_seq_t = int64_t;
type db_pgno_t = u_int32_t;	/* Page number type. */
type db_indx_t = u_int16_t;	/* Page offset type. */
pub const DB_MAX_PAGES: u32 = 0xffffffff;
type db_recno_t = u_int32_t;	/* Record number type. */
pub const DB_MAX_RECORDS: u32 = 0xffffffff;
type db_timeout_t = u_int32_t;	/* Type of a timeout. */
type roff_t = uintptr_t;
enum __db_cipher {}
enum __db_mpool {}
enum __db_rep {}
enum __db_txnmgr {}
enum __dbc_internal {}
enum __fh_t {}
enum __fname {}
enum __mpoolfile {}
enum __mutex_t {}
pub type DB = __db;
type DB_BTREE_STAT = __db_bt_stat;
type DB_CIPHER = __db_cipher;
pub type DBT = __db_dbt;
pub type DB_ENV = __db_env;
type DB_HASH_STAT = __db_h_stat;
type DB_LOCK_ILOCK = __db_ilock;
type DB_LOCK_STAT = __db_lock_stat;
type DB_LOCK = __db_lock_u;
type DB_LOCKREQ = __db_lockreq;
type DB_LOGC = __db_log_cursor;
type DB_LOG_STAT = __db_log_stat;
pub type DB_LSN = __db_lsn;
type DB_MPOOL = __db_mpool;
type DB_MPOOL_FSTAT = __db_mpool_fstat;
type DB_MPOOL_STAT = __db_mpool_stat;
type DB_MPOOLFILE = __db_mpoolfile;
type DB_PREPLIST = __db_preplist;
type DB_QUEUE_STAT = __db_qam_stat;
type DB_REP = __db_rep;
type DB_REP_STAT = __db_rep_stat;
pub type DB_SEQUENCE = __db_sequence;
type DB_SEQ_RECORD = __db_seq_record;
type DB_SEQUENCE_STAT = __db_seq_stat;
pub type DB_TXN = __db_txn;
type DB_TXN_ACTIVE = __db_txn_active;
type DB_TXN_STAT = __db_txn_stat;
type DB_TXNMGR = __db_txnmgr;
pub type DBC = __dbc;
type DBC_INTERNAL = __dbc_internal;
type DB_FH = __fh_t;
type FNAME = __fname;
type DB_KEY_RANGE = __key_range;
type MPOOLFILE = __mpoolfile;
type DB_MUTEX = __mutex_t;
pub const DB_DBT_APPMALLOC: u32 = 0x001;
pub const DB_DBT_ISSET: u32 = 0x002;
pub const DB_DBT_MALLOC: u32 = 0x004;
pub const DB_DBT_PARTIAL: u32 = 0x008;
pub const DB_DBT_REALLOC: u32 = 0x010;
pub const DB_DBT_USERMEM: u32 = 0x020;
pub const DB_DBT_DUPOK: u32 = 0x040;
#[repr(C)]
pub struct __db_dbt {
    pub data: *mut void,
    pub size: u_int32_t,
    pub ulen: u_int32_t,
    pub dlen: u_int32_t,
    pub doff: u_int32_t,
    pub flags: u_int32_t,
}
pub const DB_CREATE: u32 = 0x0000001;
pub const DB_CXX_NO_EXCEPTIONS: u32 = 0x0000002;
pub const DB_FORCE: u32 = 0x0000004;
pub const DB_NOMMAP: u32 = 0x0000008;
pub const DB_RDONLY: u32 = 0x0000010;
pub const DB_RECOVER: u32 = 0x0000020;
pub const DB_THREAD: u32 = 0x0000040;
pub const DB_TRUNCATE: u32 = 0x0000080;
pub const DB_TXN_NOSYNC: u32 = 0x0000100;
pub const DB_TXN_NOT_DURABLE: u32 = 0x0000200;
pub const DB_USE_ENVIRON: u32 = 0x0000400;
pub const DB_USE_ENVIRON_ROOT: u32 = 0x0000800;
pub const DB_AUTO_COMMIT: u32 = 0x01000000;
pub const DB_DEGREE_2: u32 = 0x02000000;
pub const DB_DIRTY_READ: u32 = 0x04000000;
pub const DB_NO_AUTO_COMMIT: u32 = 0x08000000;
pub const DB_RPCCLIENT: u32 = 0x0000001;
pub const DB_REP_CREATE: u32 = 0x0000001;
pub const DB_XA_CREATE: u32 = 0x0000002;
pub const DB_INIT_CDB: u32 = 0x0001000;
pub const DB_INIT_LOCK: u32 = 0x0002000;
pub const DB_INIT_LOG: u32 = 0x0004000;
pub const DB_INIT_MPOOL: u32 = 0x0008000;
pub const DB_INIT_REP: u32 = 0x0010000;
pub const DB_INIT_TXN: u32 = 0x0020000;
pub const DB_JOINENV: u32 = 0x0040000;
pub const DB_LOCKDOWN: u32 = 0x0080000;
pub const DB_PRIVATE: u32 = 0x0100000;
pub const DB_RECOVER_FATAL: u32 = 0x0200000;
pub const DB_SYSTEM_MEM: u32 = 0x0400000;
pub const DB_EXCL: u32 = 0x0001000;
pub const DB_FCNTL_LOCKING: u32 = 0x0002000;
pub const DB_RDWRMASTER: u32 = 0x0004000;
pub const DB_WRITEOPEN: u32 = 0x0008000;
pub const DB_TXN_NOWAIT: u32 = 0x0001000;
pub const DB_TXN_SYNC: u32 = 0x0002000;
pub const DB_ENCRYPT_AES: u32 = 0x0000001;
pub const DB_CDB_ALLDB: u32 = 0x00001000;
pub const DB_DIRECT_DB: u32 = 0x00002000;
pub const DB_DIRECT_LOG: u32 = 0x00004000;
pub const DB_DSYNC_LOG: u32 = 0x00008000;
pub const DB_LOG_AUTOREMOVE: u32 = 0x00010000;
pub const DB_LOG_INMEMORY: u32 = 0x00020000;
pub const DB_NOLOCKING: u32 = 0x00040000;
pub const DB_NOPANIC: u32 = 0x00080000;
pub const DB_OVERWRITE: u32 = 0x00100000;
pub const DB_PANIC_ENVIRONMENT: u32 = 0x00200000;
pub const DB_REGION_INIT: u32 = 0x00400000;
pub const DB_TIME_NOTGRANTED: u32 = 0x00800000;
pub const DB_TXN_WRITE_NOSYNC: u32 = 0x10000000;
pub const DB_YIELDCPU: u32 = 0x20000000;
pub const DB_UPGRADE: u32 = 0x0000001;
pub const DB_VERIFY: u32 = 0x0000002;
pub const DB_DIRECT: u32 = 0x0001000;
pub const DB_DURABLE_UNKNOWN: u32 = 0x0002000;
pub const DB_EXTENT: u32 = 0x0004000;
pub const DB_ODDFILESIZE: u32 = 0x0008000;
pub const DB_CHKSUM: u32 = 0x0000001;
pub const DB_DUP: u32 = 0x0000002;
pub const DB_DUPSORT: u32 = 0x0000004;
pub const DB_ENCRYPT: u32 = 0x0000008;
pub const DB_INORDER: u32 = 0x0000010;
pub const DB_RECNUM: u32 = 0x0000020;
pub const DB_RENUMBER: u32 = 0x0000040;
pub const DB_REVSPLITOFF: u32 = 0x0000080;
pub const DB_SNAPSHOT: u32 = 0x0000100;
pub const DB_STAT_ALL: u32 = 0x0000001;
pub const DB_STAT_CLEAR: u32 = 0x0000002;
pub const DB_STAT_LOCK_CONF: u32 = 0x0000004;
pub const DB_STAT_LOCK_LOCKERS: u32 = 0x0000008;
pub const DB_STAT_LOCK_OBJECTS: u32 = 0x0000010;
pub const DB_STAT_LOCK_PARAMS: u32 = 0x0000020;
pub const DB_STAT_MEMP_HASH: u32 = 0x0000040;
pub const DB_STAT_SUBSYSTEM: u32 = 0x0000080;
pub const DB_JOIN_NOSORT: u32 = 0x0000001;
pub const DB_AGGRESSIVE: u32 = 0x0000001;
pub const DB_NOORDERCHK: u32 = 0x0000002;
pub const DB_ORDERCHKONLY: u32 = 0x0000004;
pub const DB_PR_PAGE: u32 = 0x0000008;
pub const DB_PR_RECOVERYTEST: u32 = 0x0000010;
pub const DB_PRINTABLE: u32 = 0x0000020;
pub const DB_SALVAGE: u32 = 0x0000040;
pub const DB_UNREF: u32 = 0x0000080;
pub const DB_REP_NOBUFFER: u32 = 0x0000001;
pub const DB_REP_PERMANENT: u32 = 0x0000002;
pub const DB_LOCKVERSION: u32 = 1;
pub const DB_FILE_ID_LEN: usize = 20;
pub const DB_LOCK_NORUN: u32 = 0;
pub const DB_LOCK_DEFAULT: u32 = 1;
pub const DB_LOCK_EXPIRE: u32 = 2;
pub const DB_LOCK_MAXLOCKS: u32 = 3;
pub const DB_LOCK_MAXWRITE: u32 = 4;
pub const DB_LOCK_MINLOCKS: u32 = 5;
pub const DB_LOCK_MINWRITE: u32 = 6;
pub const DB_LOCK_OLDEST: u32 = 7;
pub const DB_LOCK_RANDOM: u32 = 8;
pub const DB_LOCK_YOUNGEST: u32 = 9;
pub const DB_LOCK_ABORT: u32 = 0x001;
pub const DB_LOCK_NOWAIT: u32 = 0x002;
pub const DB_LOCK_RECORD: u32 = 0x004;
pub const DB_LOCK_REMOVE: u32 = 0x008;
pub const DB_LOCK_SET_TIMEOUT: u32 = 0x010;
pub const DB_LOCK_SWITCH: u32 = 0x020;
pub const DB_LOCK_UPGRADE: u32 = 0x040;
#[repr(C)]
pub enum db_lockmode_t {
    DB_LOCK_NG = 0,
    DB_LOCK_READ = 1,
    DB_LOCK_WRITE = 2,
    DB_LOCK_WAIT = 3,
    DB_LOCK_IWRITE = 4,
    DB_LOCK_IREAD = 5,
    DB_LOCK_IWR = 6,
    DB_LOCK_DIRTY = 7,
    DB_LOCK_WWRITE = 8,
}
#[repr(C)]
pub enum db_lockop_t {
    DB_LOCK_DUMP = 0,
    DB_LOCK_GET = 1,
    DB_LOCK_GET_TIMEOUT = 2,
    DB_LOCK_INHERIT = 3,
    DB_LOCK_PUT = 4,
    DB_LOCK_PUT_ALL = 5,
    DB_LOCK_PUT_OBJ = 6,
    DB_LOCK_PUT_READ = 7,
    DB_LOCK_TIMEOUT = 8,
    DB_LOCK_TRADE = 9,
    DB_LOCK_UPGRADE_WRITE = 10,
}
#[repr(C)]
pub enum db_status_t {
    DB_LSTAT_ABORTED = 1,
    DB_LSTAT_EXPIRED = 2,
    DB_LSTAT_FREE = 3,
    DB_LSTAT_HELD = 4,
    DB_LSTAT_NOTEXIST = 5, /* Object on which lock was waiting
                            * was removed */
    DB_LSTAT_PENDING = 6, /* Lock was waiting and has been
                           * promoted; waiting for the owner
                           * to run and upgrade it to held. */
    DB_LSTAT_WAITING = 7,
}
#[repr(C)]
struct __db_lock_stat {
    st_id: u_int32_t,
    st_cur_maxid: u_int32_t,
    st_maxlocks: u_int32_t,
    st_maxlockers: u_int32_t,
    st_maxobjects: u_int32_t,
    st_nmodes: int,
    st_nlocks: u_int32_t,
    st_maxnlocks: u_int32_t,
    st_nlockers: u_int32_t,
    st_maxnlockers: u_int32_t,
    st_nobjects: u_int32_t,
    st_maxnobjects: u_int32_t,
    st_nconflicts: u_int32_t,
    st_nrequests: u_int32_t,
    st_nreleases: u_int32_t,
    st_nnowaits: u_int32_t, /* Number of requests that would have
                             * waited, but NOWAIT was set. */
    st_ndeadlocks: u_int32_t,
    st_locktimeout: db_timeout_t,
    st_nlocktimeouts: u_int32_t,
    st_txntimeout: db_timeout_t,
    st_ntxntimeouts: u_int32_t,
    st_region_wait: u_int32_t,
    st_region_nowait: u_int32_t,
    st_regsize: roff_t,
}
pub const DB_HANDLE_LOCK: u32 = 1;
pub const DB_RECORD_LOCK: u32 = 2;
pub const DB_PAGE_LOCK: u32 = 3;
#[repr(C)]
struct __db_ilock {
    pgno: db_pgno_t,
    fileid: [u_int8_t; DB_FILE_ID_LEN],
    ilock_type: u_int32_t,
}
#[repr(C)]
struct __db_lock_u {
    off: roff_t,
    ndx: u_int32_t, /* Index of the object referenced by
                     * this lock; used for locking. */
    gen: u_int32_t,
    mode: db_lockmode_t,
}
#[repr(C)]
struct __db_lockreq {
    op: db_lockop_t,
    mode: db_lockmode_t,
    timeout: db_timeout_t,
    obj: *mut DBT,
    lock: DB_LOCK,
}
pub const DB_LOGVERSION: u32 = 10;
pub const DB_LOGOLDVER: u32 = 10;
pub const DB_LOGMAGIC: u32 = 0x040988;
pub const DB_ARCH_ABS: u32 = 0x001;
pub const DB_ARCH_DATA: u32 = 0x002;
pub const DB_ARCH_LOG: u32 = 0x004;
pub const DB_ARCH_REMOVE: u32 = 0x008;
pub const DB_FLUSH: u32 = 0x001;
pub const DB_LOG_CHKPNT: u32 = 0x002;
pub const DB_LOG_COMMIT: u32 = 0x004;
pub const DB_LOG_NOCOPY: u32 = 0x008;
pub const DB_LOG_NOT_DURABLE: u32 = 0x010;
pub const DB_LOG_PERM: u32 = 0x020;
pub const DB_LOG_RESEND: u32 = 0x040;
pub const DB_LOG_WRNOSYNC: u32 = 0x080;
#[repr(C)]
pub struct __db_lsn {
    file: u_int32_t,
    offset: u_int32_t,
}
#[allow(non_upper_case_globals)]
pub const DB_user_BEGIN: u32 = 10000;
#[allow(non_upper_case_globals)]
pub const DB_debug_FLAG: u32 = 0x80000000;
pub const DB_LOGC_BUF_SIZE: u32 = (32 * 1024);
pub const DB_LOG_DISK: u32 = 0x01;
pub const DB_LOG_LOCKED: u32 = 0x02;
pub const DB_LOG_SILENT_ERR: u32 = 0x04;
#[repr(C)]
struct __db_log_cursor {
    dbenv: *mut DB_ENV,
    c_fhp: *mut DB_FH,
    c_lsn: DB_LSN,
    c_len: u_int32_t,
    c_prev: u_int32_t,
    c_dbt: DBT,
    bp: *mut u_int8_t,
    bp_size: u_int32_t,
    bp_rlen: u_int32_t,
    bp_lsn: DB_LSN,
    bp_maxrec: u_int32_t,
    close: extern "C" fn(*mut DB_LOGC, u_int32_t) -> int,
    get: extern "C" fn(*mut DB_LOGC, *mut DB_LSN, *mut DBT, u_int32_t) -> int,
    flags: u_int32_t,
}
#[repr(C)]
struct __db_log_stat {
    st_magic: u_int32_t,
    st_version: u_int32_t,
    st_mode: int,
    st_lg_bsize: u_int32_t,
    st_lg_size: u_int32_t,
    st_w_bytes: u_int32_t,
    st_w_mbytes: u_int32_t,
    st_wc_bytes: u_int32_t,
    st_wc_mbytes: u_int32_t,
    st_wcount: u_int32_t,
    st_wcount_fill: u_int32_t,
    st_scount: u_int32_t,
    st_region_wait: u_int32_t,
    st_region_nowait: u_int32_t,
    st_cur_file: u_int32_t,
    st_cur_offset: u_int32_t,
    st_disk_file: u_int32_t,
    st_disk_offset: u_int32_t,
    st_regsize: roff_t,
    st_maxcommitperflush: u_int32_t,
    st_mincommitperflush: u_int32_t,
}
macro_rules! DB_SET_BEGIN_LSNP {
    ($txn:expr, $rlsnp:expr) => (($txn)->set_begin_lsnp($txn, $rlsnp))
}
pub const DB_MPOOL_CREATE: u32 = 0x001;
pub const DB_MPOOL_LAST: u32 = 0x002;
pub const DB_MPOOL_NEW: u32 = 0x004;
pub const DB_MPOOL_CLEAN: u32 = 0x001;
pub const DB_MPOOL_DIRTY: u32 = 0x002;
pub const DB_MPOOL_DISCARD: u32 = 0x004;
pub const DB_MPOOL_FREE: u32 = 0x008;
pub const DB_MPOOL_NOFILE: u32 = 0x001;
pub const DB_MPOOL_UNLINK: u32 = 0x002;
#[repr(C)]
pub enum DB_CACHE_PRIORITY {
    DB_PRIORITY_VERY_LOW = 1,
    DB_PRIORITY_LOW = 2,
    DB_PRIORITY_DEFAULT = 3,
    DB_PRIORITY_HIGH = 4,
    DB_PRIORITY_VERY_HIGH = 5,
}
#[repr(C)]
struct __q {
    tqe_next: *mut __db_mpoolfile,
    tqe_prev: *mut *mut __db_mpoolfile,
}
pub const MP_FILEID_SET: u32 = 0x001;
pub const MP_FLUSH: u32 = 0x002;
pub const MP_OPEN_CALLED: u32 = 0x004;
pub const MP_READONLY: u32 = 0x008;
#[repr(C)]
struct __db_mpoolfile {
    fhp: *mut DB_FH,
    mpoolfile_ref: u_int32_t,
    pinref: u_int32_t,
    q: __q,
    dbenv: *mut DB_ENV,
    mfp: *mut MPOOLFILE,
    clear_len: u_int32_t,
    fileid: [u_int8_t; DB_FILE_ID_LEN],
    ftype: int,
    lsn_offset: int32_t,
    gbytes: u_int32_t,
    bytes: u_int32_t,
    pgcookie: *mut DBT,
    priority: DB_CACHE_PRIORITY,
    addr: *mut void,
    len: size_t,
    config_flags: u_int32_t,
    close: extern "C" fn(*mut DB_MPOOLFILE, u_int32_t) -> int,
    get: extern "C" fn(*mut DB_MPOOLFILE, *mut db_pgno_t, u_int32_t, *mut void) -> int,
    open: extern "C" fn(*mut DB_MPOOLFILE, *const char, u_int32_t, int, size_t) -> int,
    put: extern "C" fn(*mut DB_MPOOLFILE, *mut void, u_int32_t) -> int,
    set: extern "C" fn(*mut DB_MPOOLFILE, *mut void, u_int32_t) -> int,
    get_clear_len: extern "C" fn(*mut DB_MPOOLFILE, *mut u_int32_t) -> int,
    set_clear_len: extern "C" fn(*mut DB_MPOOLFILE, u_int32_t) -> int,
    get_fileid: extern "C" fn(*mut DB_MPOOLFILE, *mut u_int8_t) -> int,
    set_fileid: extern "C" fn(*mut DB_MPOOLFILE, *mut u_int8_t) -> int,
    get_flags: extern "C" fn(*mut DB_MPOOLFILE, *mut u_int32_t) -> int,
    set_flags: extern "C" fn(*mut DB_MPOOLFILE, u_int32_t, int) -> int,
    get_ftype: extern "C" fn(*mut DB_MPOOLFILE, *mut int) -> int,
    set_ftype: extern "C" fn(*mut DB_MPOOLFILE, int) -> int,
    get_lsn_offset: extern "C" fn(*mut DB_MPOOLFILE, *mut int32_t) -> int,
    set_lsn_offset: extern "C" fn(*mut DB_MPOOLFILE, int32_t) -> int,
    get_maxsize: extern "C" fn(*mut DB_MPOOLFILE, *mut u_int32_t, *mut u_int32_t) -> int,
    set_maxsize: extern "C" fn(*mut DB_MPOOLFILE, u_int32_t, u_int32_t) -> int,
    get_pgcookie: extern "C" fn(*mut DB_MPOOLFILE, *mut DBT) -> int,
    set_pgcookie: extern "C" fn(*mut DB_MPOOLFILE, *mut DBT) -> int,
    get_priority: extern "C" fn(*mut DB_MPOOLFILE, *mut DB_CACHE_PRIORITY) -> int,
    set_priority: extern "C" fn(*mut DB_MPOOLFILE, DB_CACHE_PRIORITY) -> int,
    sync: extern "C" fn(*mut DB_MPOOLFILE) -> int,
    flags: u_int32_t,
}
#[repr(C)]
struct __db_mpool_stat {
    st_gbytes: u_int32_t,
    st_bytes: u_int32_t,
    st_ncache: u_int32_t,
    st_regsize: roff_t,
    st_mmapsize: size_t,
    st_maxopenfd: int,
    st_maxwrite: int,
    st_maxwrite_sleep: int,
    st_map: u_int32_t,
    st_cache_hit: u_int32_t,
    st_cache_miss: u_int32_t,
    st_page_create: u_int32_t,
    st_page_in: u_int32_t,
    st_page_out: u_int32_t,
    st_ro_evict: u_int32_t,
    st_rw_evict: u_int32_t,
    st_page_trickle: u_int32_t,
    st_pages: u_int32_t,
    st_page_clean: u_int32_t,
    st_page_dirty: u_int32_t,
    st_hash_buckets: u_int32_t,
    st_hash_searches: u_int32_t,
    st_hash_longest: u_int32_t,
    st_hash_examined: u_int32_t,
    st_hash_nowait: u_int32_t,
    st_hash_wait: u_int32_t,
    st_hash_max_wait: u_int32_t,
    st_region_nowait: u_int32_t,
    st_region_wait: u_int32_t,
    st_alloc: u_int32_t,
    st_alloc_buckets: u_int32_t,
    st_alloc_max_buckets: u_int32_t,
    st_alloc_pages: u_int32_t,
    st_alloc_max_pages: u_int32_t,
}
#[repr(C)]
struct __db_mpool_fstat {
    file_name: *mut char,
    st_pagesize: u_int32_t,
    st_map: u_int32_t,
    st_cache_hit: u_int32_t,
    st_cache_miss: u_int32_t,
    st_page_create: u_int32_t,
    st_page_in: u_int32_t,
    st_page_out: u_int32_t,
}
pub const DB_TXNVERSION: u32 = 1;
#[repr(C)]
pub enum db_recops {
    DB_TXN_ABORT = 0,
    DB_TXN_APPLY = 1,
    DB_TXN_BACKWARD_ALLOC = 2,
    DB_TXN_BACKWARD_ROLL = 3,
    DB_TXN_FORWARD_ROLL = 4,
    DB_TXN_OPENFILES = 5,
    DB_TXN_POPENFILES = 6,
    DB_TXN_PRINT = 7,
}
macro_rules! DB_UNDO {
    ($op:expr) => (($op) == DB_TXN_ABORT || ($op) == DB_TXN_BACKWARD_ROLL || ($op) == DB_TXN_BACKWARD_ALLOC)
}
macro_rules! DB_REDO {
    ($op:expr) => ((op) == DB_TXN_FORWARD_ROLL || (op) == DB_TXN_APPLY)
}
#[repr(C)]
struct __txn_links {
    tqe_next: *mut __db_txn,
    tqe_prev: *mut *mut __db_txn,
}
#[repr(C)]
struct __xalinks {
    tqe_next: *mut __db_txn,
    tqe_prev: *mut *mut __db_txn,
}
pub enum __txn_event {}
#[repr(C)]
struct __events {
    tqh_first: *mut __txn_event,
    tqh_last: *mut *mut __txn_event,
}
pub enum __txn_logrec {}
#[repr(C)]
struct __logs {
    stqh_first: *mut __txn_logrec,
    stqh_last: *mut *mut __txn_logrec,
}
#[repr(C)]
struct __kids {
    tqh_first: *mut __db_txn,
    tqh_last: *mut *mut __db_txn,
}
#[repr(C)]
struct __klinks {
    tqe_next: *mut __db_txn,
    tqe_prev: *mut *mut __db_txn,
}
pub const TXN_CHILDCOMMIT: u32 = 0x001;
pub const TXN_COMPENSATE: u32 = 0x002;
pub const TXN_DEADLOCK: u32 = 0x004;
pub const TXN_DEGREE_2: u32 = 0x008;
pub const TXN_DIRTY_READ: u32 = 0x010;
pub const TXN_LOCKTIMEOUT: u32 = 0x020;
pub const TXN_MALLOC: u32 = 0x040;
pub const TXN_NOSYNC: u32 = 0x080;
pub const TXN_NOWAIT: u32 = 0x100;
pub const TXN_RESTORED: u32 = 0x200;
pub const TXN_SYNC: u32 = 0x400;
#[repr(C)]
pub struct __db_txn {
    mgrp: *mut DB_TXNMGR,
    parent: *mut DB_TXN,
    last_lsn: DB_LSN,
    txnid: u_int32_t,
    tid: u_int32_t,
    off: roff_t,
    lock_timeout: db_timeout_t,
    expire: db_timeout_t,
    txn_list: *mut void,
    links: __txn_links,
    xalinks: __xalinks,
    events: __events,
    logs: __logs,
    kids: __kids,
    klinks: __klinks,
    api_internal: *mut void,
    xml_internal: *mut void,
    cursors: u_int32_t,
    abort: extern "C" fn(*mut DB_TXN) -> int,
    commit: extern "C" fn(*mut DB_TXN, u_int32_t) -> int,
    discard: extern "C" fn(*mut DB_TXN, u_int32_t) -> int,
    id: extern "C" fn(*mut DB_TXN) -> u_int32_t,
    prepare: extern "C" fn(*mut DB_TXN, *mut u_int8_t) -> int,
    set_begin_lsnp: extern "C" fn(*mut DB_TXN, *mut *mut DB_LSN) -> void,
    set_timeout: extern "C" fn(*mut DB_TXN, db_timeout_t, u_int32_t) -> int,
    flags: u_int32_t,
}
pub const DB_XIDDATASIZE: usize = 128;
#[repr(C)]
struct __db_preplist {
    txn: *mut DB_TXN,
    gid: [u_int8_t; DB_XIDDATASIZE],
}
#[repr(C)]
struct __db_txn_active {
    txnid: u_int32_t,
    parentid: u_int32_t,
    lsn: DB_LSN,
    xa_status: u_int32_t,
    xid: [u_int8_t; DB_XIDDATASIZE],
}
#[repr(C)]
struct __db_txn_stat {
    st_last_ckp: DB_LSN,
    st_time_ckp: time_t,
    st_last_txnid: u_int32_t,
    st_maxtxns: u_int32_t,
    st_naborts: u_int32_t,
    st_nbegins: u_int32_t,
    st_ncommits: u_int32_t,
    st_nactive: u_int32_t,
    st_nrestores: u_int32_t, /* number of restored transactions
                              * after recovery. */
    st_maxnactive: u_int32_t,
    st_txnarray: *mut DB_TXN_ACTIVE,
    st_region_wait: u_int32_t,
    st_region_nowait: u_int32_t,
    st_regsize: roff_t,
}
pub const DB_EID_BROADCAST: i32 = -1;
pub const DB_EID_INVALID: i32 = -2;
pub const DB_REP_CLIENT: u32 = 0x001;
pub const DB_REP_MASTER: u32 = 0x002;
#[repr(C)]
struct __db_rep_stat {
    st_status: u_int32_t,
    st_next_lsn: DB_LSN,
    st_waiting_lsn: DB_LSN,
    st_next_pg: db_pgno_t,
    st_waiting_pg: db_pgno_t,
    st_dupmasters: u_int32_t, /* # of times a duplicate master
                               * condition was detected.+ */
    st_env_id: int,
    st_env_priority: int,
    st_gen: u_int32_t,
    st_egen: u_int32_t,
    st_log_duplicated: u_int32_t,
    st_log_queued: u_int32_t,
    st_log_queued_max: u_int32_t,
    st_log_queued_total: u_int32_t,
    st_log_records: u_int32_t,
    st_log_requested: u_int32_t,
    st_master: int,
    st_master_changes: u_int32_t,
    st_msgs_badgen: u_int32_t,
    st_msgs_processed: u_int32_t,
    st_msgs_recover: u_int32_t, /* Messages ignored because this site
                                 * was a client in recovery.+ */
    st_msgs_send_failures: u_int32_t,
    st_msgs_sent: u_int32_t,
    st_newsites: u_int32_t,
    st_nsites: int, /* Current number of sites we will
                     * assume during elections. */
    st_nthrottles: u_int32_t,
    st_outdated: u_int32_t, /* # of times we detected and returned
                             * an OUTDATED condition.+ */
    st_pg_duplicated: u_int32_t,
    st_pg_records: u_int32_t,
    st_pg_requested: u_int32_t,
    st_startup_complete: u_int32_t,
    st_txns_applied: u_int32_t,
    st_elections: u_int32_t,
    st_elections_won: u_int32_t,
    st_election_cur_winner: int,
    st_election_gen: u_int32_t,
    st_election_lsn: DB_LSN,
    st_election_nsites: int,
    st_election_nvotes: int,
    st_election_priority: int,
    st_election_status: int,
    st_election_tiebreaker: u_int32_t,
    st_election_votes: int,
}
pub const DB_SEQ_DEC: u32 = 0x00000001;
pub const DB_SEQ_INC: u32 = 0x00000002;
pub const DB_SEQ_RANGE_SET: u32 = 0x00000004;
pub const DB_SEQ_WRAP: u32 = 0x00000008;
#[repr(C)]
struct __db_seq_record {
    seq_version: u_int32_t,
    flags: u_int32_t,
    seq_value: db_seq_t,
    seq_max: db_seq_t,
    seq_min: db_seq_t,
}
#[repr(C)]
pub struct __db_sequence {
    seq_dbp: *mut DB,
    seq_mutexp: *mut DB_MUTEX,
    seq_rp: *mut DB_SEQ_RECORD,
    seq_record: DB_SEQ_RECORD,
    seq_cache_size: int32_t,
    seq_last_value: db_seq_t,
    seq_key: DBT,
    seq_data: DBT,
    api_internal: *mut void,
    close: extern "C" fn(*mut DB_SEQUENCE, u_int32_t) -> int,
    get: extern "C" fn(*mut DB_SEQUENCE,
                       *mut DB_TXN,
                       int32_t,
                       *mut db_seq_t,
                       u_int32_t)
                       -> int,
    get_cachesize: extern "C" fn(*mut DB_SEQUENCE, *mut int32_t) -> int,
    get_db: extern "C" fn(*mut DB_SEQUENCE, *mut *mut DB) -> int,
    get_flags: extern "C" fn(*mut DB_SEQUENCE, *mut u_int32_t) -> int,
    get_key: extern "C" fn(*mut DB_SEQUENCE, *mut DBT) -> int,
    get_range: extern "C" fn(*mut DB_SEQUENCE, *mut db_seq_t, *mut db_seq_t) -> int,
    initial_value: extern "C" fn(*mut DB_SEQUENCE, db_seq_t) -> int,
    open: extern "C" fn(*mut DB_SEQUENCE, *mut DB_TXN, *mut DBT, u_int32_t) -> int,
    remove: extern "C" fn(*mut DB_SEQUENCE, *mut DB_TXN, u_int32_t) -> int,
    set_cachesize: extern "C" fn(*mut DB_SEQUENCE, int32_t) -> int,
    set_flags: extern "C" fn(*mut DB_SEQUENCE, u_int32_t) -> int,
    set_range: extern "C" fn(*mut DB_SEQUENCE, db_seq_t, db_seq_t) -> int,
    stat: extern "C" fn(*mut DB_SEQUENCE, *mut *mut DB_SEQUENCE_STAT, u_int32_t) -> int,
    stat_print: extern "C" fn(*mut DB_SEQUENCE, u_int32_t) -> int,
}
#[repr(C)]
struct __db_seq_stat {
    st_wait: u_int32_t,
    st_nowait: u_int32_t,
    st_current: db_seq_t,
    st_value: db_seq_t,
    st_last_value: db_seq_t,
    st_min: db_seq_t,
    st_max: db_seq_t,
    st_cache_size: int32_t,
    st_flags: u_int32_t,
}
#[repr(C)]
pub enum DBTYPE {
    DB_BTREE = 1,
    DB_HASH = 2,
    DB_RECNO = 3,
    DB_QUEUE = 4,
    DB_UNKNOWN = 5,
}
pub const DB_RENAMEMAGIC: u32 = 0x030800;
pub const DB_BTREEVERSION: u32 = 9;
pub const DB_BTREEOLDVER: u32 = 8;
pub const DB_BTREEMAGIC: u32 = 0x053162;
pub const DB_HASHVERSION: u32 = 8;
pub const DB_HASHOLDVER: u32 = 7;
pub const DB_HASHMAGIC: u32 = 0x061561;
pub const DB_QAMVERSION: u32 = 4;
pub const DB_QAMOLDVER: u32 = 3;
pub const DB_QAMMAGIC: u32 = 0x042253;
pub const DB_SEQUENCE_VERSION: u32 = 2;
pub const DB_SEQUENCE_OLDVER: u32 = 1;
pub const DB_AFTER: u32 = 1;
pub const DB_APPEND: u32 = 2;
pub const DB_BEFORE: u32 = 3;
pub const DB_CACHED_COUNTS: u32 = 4;
pub const DB_CONSUME: u32 = 5;
pub const DB_CONSUME_WAIT: u32 = 6;
pub const DB_CURRENT: u32 = 7;
pub const DB_FAST_STAT: u32 = 8;
pub const DB_FIRST: u32 = 9;
pub const DB_GET_BOTH: u32 = 10;
pub const DB_GET_BOTHC: u32 = 11;
pub const DB_GET_BOTH_RANGE: u32 = 12;
pub const DB_GET_RECNO: u32 = 13;
pub const DB_JOIN_ITEM: u32 = 14;
pub const DB_KEYFIRST: u32 = 15;
pub const DB_KEYLAST: u32 = 16;
pub const DB_LAST: u32 = 17;
pub const DB_NEXT: u32 = 18;
pub const DB_NEXT_DUP: u32 = 19;
pub const DB_NEXT_NODUP: u32 = 20;
pub const DB_NODUPDATA: u32 = 21;
pub const DB_NOOVERWRITE: u32 = 22;
pub const DB_NOSYNC: u32 = 23;
pub const DB_POSITION: u32 = 24;
pub const DB_PREV: u32 = 25;
pub const DB_PREV_NODUP: u32 = 26;
pub const DB_RECORDCOUNT: u32 = 27;
pub const DB_SET: u32 = 28;
pub const DB_SET_LOCK_TIMEOUT: u32 = 29;
pub const DB_SET_RANGE: u32 = 30;
pub const DB_SET_RECNO: u32 = 31;
pub const DB_SET_TXN_NOW: u32 = 32;
pub const DB_SET_TXN_TIMEOUT: u32 = 33;
pub const DB_UPDATE_SECONDARY: u32 = 34;
pub const DB_WRITECURSOR: u32 = 35;
pub const DB_WRITELOCK: u32 = 36;
pub const DB_OPFLAGS_MASK: u32 = 0x000000ff;
pub const DB_MULTIPLE: u32 = 0x08000000;
pub const DB_MULTIPLE_KEY: u32 = 0x10000000;
pub const DB_RMW: u32 = 0x20000000;
pub const DB_BUFFER_SMALL: i32 = -30999;
pub const DB_DONOTINDEX: i32 = -30998;
pub const DB_KEYEMPTY: i32 = -30997;
pub const DB_KEYEXIST: i32 = -30996;
pub const DB_LOCK_DEADLOCK: i32 = -30995;
pub const DB_LOCK_NOTGRANTED: i32 = -30994;
pub const DB_LOG_BUFFER_FULL: i32 = -30993;
pub const DB_NOSERVER: i32 = -30992;
pub const DB_NOSERVER_HOME: i32 = -30991;
pub const DB_NOSERVER_ID: i32 = -30990;
pub const DB_NOTFOUND: i32 = -30989;
pub const DB_OLD_VERSION: i32 = -30988;
pub const DB_PAGE_NOTFOUND: i32 = -30987;
pub const DB_REP_DUPMASTER: i32 = -30986;
pub const DB_REP_HANDLE_DEAD: i32 = -30985;
pub const DB_REP_HOLDELECTION: i32 = -30984;
pub const DB_REP_ISPERM: i32 = -30983;
pub const DB_REP_NEWMASTER: i32 = -30982;
pub const DB_REP_NEWSITE: i32 = -30981;
pub const DB_REP_NOTPERM: i32 = -30980;
pub const DB_REP_STARTUPDONE: i32 = -30979;
pub const DB_REP_UNAVAIL: i32 = -30978;
pub const DB_RUNRECOVERY: i32 = -30977;
pub const DB_SECONDARY_BAD: i32 = -30976;
pub const DB_VERIFY_BAD: i32 = -30975;
pub const DB_VERSION_MISMATCH: i32 = -30974;
pub const DB_ALREADY_ABORTED: i32 = -30899;
pub const DB_DELETED: i32 = -30898;
pub const DB_LOCK_NOTEXIST: i32 = -30897;
pub const DB_NEEDSPLIT: i32 = -30896;
pub const DB_REP_EGENCHG: i32 = -30895;
pub const DB_REP_LOGREADY: i32 = -30894;
pub const DB_REP_PAGEDONE: i32 = -30893;
pub const DB_SURPRISE_KID: i32 = -30892;
pub const DB_SWAPBYTES: i32 = -30891;
pub const DB_TIMEOUT: i32 = -30890;
pub const DB_TXN_CKP: i32 = -30889;
pub const DB_VERIFY_FATAL: i32 = -30888;
pub const DB_LOGFILEID_INVALID: i32 = -1;
#[repr(C)]
struct __dblistlinks {
    le_next: *mut __db,
    le_prev: *mut *mut __db,
}
#[repr(C)]
struct __cq_fq {
    tqh_first: *mut __dbc,
    tqh_last: *mut *mut __dbc,
}
#[repr(C)]
struct __cq_aq {
    tqh_first: *mut __dbc,
    tqh_last: *mut *mut __dbc,
}
#[repr(C)]
struct __cq_jq {
    tqh_first: *mut __dbc,
    tqh_last: *mut *mut __dbc,
}
#[repr(C)]
struct __s_secondaries {
    lh_first: *mut __db,
}
#[repr(C)]
struct __s_links {
    le_next: *mut __db,
    le_prev: *mut *mut __db,
}
pub const DB_OK_BTREE: u32 = 0x01;
pub const DB_OK_HASH: u32 = 0x02;
pub const DB_OK_QUEUE: u32 = 0x04;
pub const DB_OK_RECNO: u32 = 0x08;
pub const DB_AM_CHKSUM: u32 = 0x00000001;
pub const DB_AM_CL_WRITER: u32 = 0x00000002;
pub const DB_AM_COMPENSATE: u32 = 0x00000004;
pub const DB_AM_CREATED: u32 = 0x00000008;
pub const DB_AM_CREATED_MSTR: u32 = 0x00000010;
pub const DB_AM_DBM_ERROR: u32 = 0x00000020;
pub const DB_AM_DELIMITER: u32 = 0x00000040;
pub const DB_AM_DIRTY: u32 = 0x00000080;
pub const DB_AM_DISCARD: u32 = 0x00000100;
pub const DB_AM_DUP: u32 = 0x00000200;
pub const DB_AM_DUPSORT: u32 = 0x00000400;
pub const DB_AM_ENCRYPT: u32 = 0x00000800;
pub const DB_AM_FIXEDLEN: u32 = 0x00001000;
pub const DB_AM_INMEM: u32 = 0x00002000;
pub const DB_AM_INORDER: u32 = 0x00004000;
pub const DB_AM_IN_RENAME: u32 = 0x00008000;
pub const DB_AM_NOT_DURABLE: u32 = 0x00010000;
pub const DB_AM_OPEN_CALLED: u32 = 0x00020000;
pub const DB_AM_PAD: u32 = 0x00040000;
pub const DB_AM_PGDEF: u32 = 0x00080000;
pub const DB_AM_RDONLY: u32 = 0x00100000;
pub const DB_AM_RECNUM: u32 = 0x00200000;
pub const DB_AM_RECOVER: u32 = 0x00400000;
pub const DB_AM_RENUMBER: u32 = 0x00800000;
pub const DB_AM_REPLICATION: u32 = 0x01000000;
pub const DB_AM_REVSPLITOFF: u32 = 0x02000000;
pub const DB_AM_SECONDARY: u32 = 0x04000000;
pub const DB_AM_SNAPSHOT: u32 = 0x08000000;
pub const DB_AM_SUBDB: u32 = 0x10000000;
pub const DB_AM_SWAP: u32 = 0x20000000;
pub const DB_AM_TXN: u32 = 0x40000000;
pub const DB_AM_VERIFYING: u32 = 0x80000000;
#[repr(C)]
pub struct __db {
    pgsize: u_int32_t,
    db_append_recno: extern "C" fn(*mut DB, *mut DBT, db_recno_t) -> int,
    db_feedback: extern "C" fn(*mut DB, int, int) -> void,
    dup_compare: extern "C" fn(*mut DB, *const DBT, *const DBT) -> int,
    app_private: *mut void,
    dbenv: *mut DB_ENV,
    db_type: DBTYPE,
    mpf: *mut DB_MPOOLFILE,
    mutexp: *mut DB_MUTEX,
    fname: *mut char,
    dname: *mut char,
    open_flags: u_int32_t,
    fileid: [u_int8_t; DB_FILE_ID_LEN],
    adj_fileid: u_int32_t,
    log_filename: *mut FNAME,
    meta_pgno: db_pgno_t,
    lid: u_int32_t,
    cur_lid: u_int32_t,
    associate_lid: u_int32_t,
    handle_lock: DB_LOCK,
    cl_id: u_int,
    timestamp: time_t,
    my_rskey: DBT,
    my_rkey: DBT,
    my_rdata: DBT,
    saved_open_fhp: *mut DB_FH,
    dblistlinks: __dblistlinks,
    free_queue: __cq_fq,
    active_queue: __cq_aq,
    join_queue: __cq_jq,
    s_secondaries: __s_secondaries,
    s_links: __s_links,
    s_refcnt: u_int32_t,
    s_callback: extern "C" fn(*mut DB, *const DBT, *const DBT, *mut DBT) -> int,
    s_primary: *mut DB,
    api_internal: *mut void,
    bt_internal: *mut void,
    h_internal: *mut void,
    q_internal: *mut void,
    xa_internal: *mut void,
    associate: extern "C" fn(*mut DB,
                             *mut DB_TXN,
                             *mut DB,
                             extern "C" fn(*mut DB, *const DBT, *const DBT, *mut DBT) -> int,
                             u_int32_t)
                             -> int,
    pub close: extern "C" fn(*mut DB, u_int32_t) -> int,
    pub cursor: extern "C" fn(*mut DB, *mut DB_TXN, *mut *mut DBC, u_int32_t) -> int,
    pub del: extern "C" fn(*mut DB, *mut DB_TXN, *mut DBT, u_int32_t) -> int,
    dump: extern "C" fn(*mut DB,
                        *const char,
                        extern "C" fn(*mut void, *const void) -> int,
                        *mut void,
                        int,
                        int)
                        -> int,
    err: extern "C" fn(*mut DB, int, *const char, ...) -> void,
    errx: extern "C" fn(*mut DB, *const char, ...) -> void,
    fd: extern "C" fn(*mut DB, *mut int) -> int,
    pub get: extern "C" fn(*mut DB, *mut DB_TXN, *mut DBT, *mut DBT, u_int32_t) -> int,
    pget: extern "C" fn(*mut DB,
                        *mut DB_TXN,
                        *mut DBT,
                        *mut DBT,
                        *mut DBT,
                        u_int32_t)
                        -> int,
    get_byteswapped: extern "C" fn(*mut DB, *mut int) -> int,
    get_cachesize: extern "C" fn(*mut DB, *mut u_int32_t, *mut u_int32_t, *mut int) -> int,
    get_dbname: extern "C" fn(*mut DB, *const *mut char, *const *mut char) -> int,
    get_encrypt_flags: extern "C" fn(*mut DB, *mut u_int32_t) -> int,
    get_env: extern "C" fn(*mut DB) -> *mut DB_ENV,
    get_errfile: extern "C" fn(*mut DB, *mut *mut FILE) -> void,
    get_errpfx: extern "C" fn(*mut DB, *const *mut char) -> void,
    get_flags: extern "C" fn(*mut DB, *mut u_int32_t) -> int,
    get_lorder: extern "C" fn(*mut DB, *mut int) -> int,
    get_open_flags: extern "C" fn(*mut DB, *mut u_int32_t) -> int,
    get_pagesize: extern "C" fn(*mut DB, *mut u_int32_t) -> int,
    get_transactional: extern "C" fn(*mut DB) -> int,
    get_type: extern "C" fn(*mut DB, *mut DBTYPE) -> int,
    join: extern "C" fn(*mut DB, *mut *mut DBC, *mut *mut DBC, u_int32_t) -> int,
    key_range: extern "C" fn(*mut DB, *mut DB_TXN, *mut DBT, *mut DB_KEY_RANGE, u_int32_t) -> int,
    pub open: extern "C" fn(*mut DB,
                            *mut DB_TXN,
                            *const char,
                            *const char,
                            DBTYPE,
                            u_int32_t,
                            int)
                            -> int,
    pub put: extern "C" fn(*mut DB, *mut DB_TXN, *mut DBT, *mut DBT, u_int32_t) -> int,
    remove: extern "C" fn(*mut DB, *const char, *const char, u_int32_t) -> int,
    rename: extern "C" fn(*mut DB, *const char, *const char, *const char, u_int32_t) -> int,
    truncate: extern "C" fn(*mut DB, *mut DB_TXN, *mut u_int32_t, u_int32_t) -> int,
    set_append_recno: extern "C" fn(*mut DB, extern "C" fn(*mut DB, *mut DBT, db_recno_t) -> int)
                                    -> int,
    set_alloc: extern "C" fn(*mut DB,
                             extern "C" fn(size_t) -> *mut void,
                             extern "C" fn(*mut void, size_t) -> *mut void,
                             extern "C" fn(*mut void) -> void)
                             -> int,
    set_cachesize: extern "C" fn(*mut DB, u_int32_t, u_int32_t, int) -> int,
    set_dup_compare: extern "C" fn(*mut DB,
                                   extern "C" fn(*mut DB, *const DBT, *const DBT) -> int)
                                   -> int,
    set_encrypt: extern "C" fn(*mut DB, *const char, u_int32_t) -> int,
    set_errcall: extern "C" fn(*mut DB,
                               extern "C" fn(*const DB_ENV, *const char, *const char) -> void)
                               -> void,
    set_errfile: extern "C" fn(*mut DB, *mut FILE) -> void,
    set_errpfx: extern "C" fn(*mut DB, *const char) -> void,
    set_feedback: extern "C" fn(*mut DB, extern "C" fn(*mut DB, int, int) -> void) -> int,
    set_flags: extern "C" fn(*mut DB, u_int32_t) -> int,
    set_lorder: extern "C" fn(*mut DB, int) -> int,
    set_msgcall: extern "C" fn(*mut DB, extern "C" fn(*const DB_ENV, *const char) -> void) -> void,
    get_msgfile: extern "C" fn(*mut DB, *mut *mut FILE) -> void,
    set_msgfile: extern "C" fn(*mut DB, *mut FILE) -> void,
    set_pagesize: extern "C" fn(*mut DB, u_int32_t) -> int,
    set_paniccall: extern "C" fn(*mut DB, extern "C" fn(*mut DB_ENV, int) -> void) -> int,
    stat: extern "C" fn(*mut DB, *mut DB_TXN, *mut void, u_int32_t) -> int,
    stat_print: extern "C" fn(*mut DB, u_int32_t) -> int,
    sync: extern "C" fn(*mut DB, u_int32_t) -> int,
    upgrade: extern "C" fn(*mut DB, *const char, u_int32_t) -> int,
    verify: extern "C" fn(*mut DB, *const char, *const char, *mut FILE, u_int32_t) -> int,
    get_bt_minkey: extern "C" fn(*mut DB, *mut u_int32_t) -> int,
    set_bt_compare: extern "C" fn(*mut DB,
                                  extern "C" fn(*mut DB, *const DBT, *const DBT) -> int)
                                  -> int,
    set_bt_maxkey: extern "C" fn(*mut DB, u_int32_t) -> int,
    set_bt_minkey: extern "C" fn(*mut DB, u_int32_t) -> int,
    set_bt_prefix: extern "C" fn(*mut DB,
                                 extern "C" fn(*mut DB, *const DBT, *const DBT) -> size_t)
                                 -> int,
    get_h_ffactor: extern "C" fn(*mut DB, *mut u_int32_t) -> int,
    get_h_nelem: extern "C" fn(*mut DB, *mut u_int32_t) -> int,
    set_h_ffactor: extern "C" fn(*mut DB, u_int32_t) -> int,
    set_h_hash: extern "C" fn(*mut DB,
                              extern "C" fn(*mut DB, *const void, u_int32_t) -> u_int32_t)
                              -> int,
    set_h_nelem: extern "C" fn(*mut DB, u_int32_t) -> int,
    get_re_delim: extern "C" fn(*mut DB, *mut int) -> int,
    get_re_len: extern "C" fn(*mut DB, *mut u_int32_t) -> int,
    get_re_pad: extern "C" fn(*mut DB, *mut int) -> int,
    get_re_source: extern "C" fn(*mut DB, *const *mut char) -> int,
    set_re_delim: extern "C" fn(*mut DB, int) -> int,
    set_re_len: extern "C" fn(*mut DB, u_int32_t) -> int,
    set_re_pad: extern "C" fn(*mut DB, int) -> int,
    set_re_source: extern "C" fn(*mut DB, *const char) -> int,
    get_q_extentsize: extern "C" fn(*mut DB, *mut u_int32_t) -> int,
    set_q_extentsize: extern "C" fn(*mut DB, u_int32_t) -> int,
    db_am_remove: extern "C" fn(*mut DB, *mut DB_TXN, *const char, *const char) -> int,
    db_am_rename: extern "C" fn(*mut DB, *mut DB_TXN, *const char, *const char, *const char) -> int,
    stored_get: extern "C" fn(*mut DB, *mut DB_TXN, *mut DBT, *mut DBT, u_int32_t) -> int,
    stored_close: extern "C" fn(*mut DB, u_int32_t) -> int,
    am_ok: u_int32_t,
    orig_flags: u_int32_t,
    flags: u_int32_t,
}
macro_rules! DB_MULTIPLE_INIT {
    ($pointer:expr, $dbt:expr) => ($pointer = (*mut u_int8_t)($dbt)->data + ($dbt)->ulen - sizeof(u_int32_t))
}
macro_rules! DB_MULTIPLE_NEXT {
    (pointer, dbt, retdata, retdlen) => {
        do {
            if (*((u_int32_t *)(pointer)) == (u_int32_t)-1) {
                retdata = NULL;
                pointer = NULL;
                break;
            }
            retdata = (u_int8_t *)
                (dbt)->data + *(u_int32_t *)(pointer);
            (pointer) = (u_int32_t *)(pointer) - 1;
            retdlen = *(u_int32_t *)(pointer);
            (pointer) = (u_int32_t *)(pointer) - 1;
            if (retdlen == 0 &&
                retdata == (u_int8_t *)(dbt)->data)
                retdata = NULL;
        } while (0)
    }
}
macro_rules! DB_MULTIPLE_KEY_NEXT {
    (pointer, dbt, retkey, retklen, retdata, retdlen) => {
        do {
            if (*((u_int32_t *)(pointer)) == (u_int32_t)-1) {
                retdata = NULL;
                retkey = NULL;
                pointer = NULL;
                break;
            }
            retkey = (u_int8_t *)
                (dbt)->data + *(u_int32_t *)(pointer);
            (pointer) = (u_int32_t *)(pointer) - 1;
            retklen = *(u_int32_t *)(pointer);
            (pointer) = (u_int32_t *)(pointer) - 1;
            retdata = (u_int8_t *)
                (dbt)->data + *(u_int32_t *)(pointer);
            (pointer) = (u_int32_t *)(pointer) - 1;
            retdlen = *(u_int32_t *)(pointer);
            (pointer) = (u_int32_t *)(pointer) - 1;
        } while (0)
    }
}
macro_rules! DB_MULTIPLE_RECNO_NEXT {
    (pointer, dbt, recno, retdata, retdlen) => {
        do {
            if (*((u_int32_t *)(pointer)) == (u_int32_t)0) {
                recno = 0;
                retdata = NULL;
                pointer = NULL;
                break;
            }
            recno = *(u_int32_t *)(pointer);
            (pointer) = (u_int32_t *)(pointer) - 1;
            retdata = (u_int8_t *)
                (dbt)->data + *(u_int32_t *)(pointer);
            (pointer) = (u_int32_t *)(pointer) - 1;
            retdlen = *(u_int32_t *)(pointer);
            (pointer) = (u_int32_t *)(pointer) - 1;
        } while (0)
    }
}
#[repr(C)]
struct __dbc_links {
    tqe_next: *mut DBC,
    tqe_prev: *mut *mut DBC,
}
pub const DBC_ACTIVE: u32 = 0x0001;
pub const DBC_COMPENSATE: u32 = 0x0002;
pub const DBC_DEGREE_2: u32 = 0x0004;
pub const DBC_DIRTY_READ: u32 = 0x0008;
pub const DBC_OPD: u32 = 0x0010;
pub const DBC_RECOVER: u32 = 0x0020;
pub const DBC_RMW: u32 = 0x0040;
pub const DBC_TRANSIENT: u32 = 0x0080;
pub const DBC_WRITECURSOR: u32 = 0x0100;
pub const DBC_WRITER: u32 = 0x0200;
pub const DBC_MULTIPLE: u32 = 0x0400;
pub const DBC_MULTIPLE_KEY: u32 = 0x0800;
pub const DBC_OWN_LID: u32 = 0x1000;
#[repr(C)]
pub struct __dbc {
    dbp: *mut DB,
    txn: *mut DB_TXN,
    links: __dbc_links,
    rskey: *mut DBT,
    rkey: *mut DBT,
    rdata: *mut DBT,
    my_rskey: DBT,
    my_rkey: DBT,
    my_rdata: DBT,
    lid: u_int32_t,
    locker: u_int32_t,
    lock_dbt: DBT,
    lock: DB_LOCK_ILOCK,
    mylock: DB_LOCK,
    cl_id: u_int,
    dbtype: DBTYPE,
    internal: *mut DBC_INTERNAL,
    pub c_close: extern "C" fn(*mut DBC) -> int,
    c_count: extern "C" fn(*mut DBC, *mut db_recno_t, u_int32_t) -> int,
    pub c_del: extern "C" fn(*mut DBC, u_int32_t) -> int,
    c_dup: extern "C" fn(*mut DBC, *mut *mut DBC, u_int32_t) -> int,
    pub c_get: extern "C" fn(*mut DBC, *mut DBT, *mut DBT, u_int32_t) -> int,
    c_pget: extern "C" fn(*mut DBC, *mut DBT, *mut DBT, *mut DBT, u_int32_t) -> int,
    pub c_put: extern "C" fn(*mut DBC, *mut DBT, *mut DBT, u_int32_t) -> int,
    c_am_bulk: extern "C" fn(*mut DBC, *mut DBT, u_int32_t) -> int,
    c_am_close: extern "C" fn(*mut DBC, db_pgno_t, *mut int) -> int,
    c_am_del: extern "C" fn(*mut DBC) -> int,
    c_am_destroy: extern "C" fn(*mut DBC) -> int,
    c_am_get: extern "C" fn(*mut DBC, *mut DBT, *mut DBT, u_int32_t, *mut db_pgno_t) -> int,
    c_am_put: extern "C" fn(*mut DBC, *mut DBT, *mut DBT, u_int32_t, *mut db_pgno_t) -> int,
    c_am_writelock: extern "C" fn(*mut DBC) -> int,
    flags: u_int32_t,
}
#[repr(C)]
struct __key_range {
    less: double,
    equal: double,
    greater: double,
}
#[repr(C)]
struct __db_bt_stat {
    bt_magic: u_int32_t,
    bt_version: u_int32_t,
    bt_metaflags: u_int32_t,
    bt_nkeys: u_int32_t,
    bt_ndata: u_int32_t,
    bt_pagesize: u_int32_t,
    bt_maxkey: u_int32_t,
    bt_minkey: u_int32_t,
    bt_re_len: u_int32_t,
    bt_re_pad: u_int32_t,
    bt_levels: u_int32_t,
    bt_int_pg: u_int32_t,
    bt_leaf_pg: u_int32_t,
    bt_dup_pg: u_int32_t,
    bt_over_pg: u_int32_t,
    bt_empty_pg: u_int32_t,
    bt_free: u_int32_t,
    bt_int_pgfree: u_int32_t,
    bt_leaf_pgfree: u_int32_t,
    bt_dup_pgfree: u_int32_t,
    bt_over_pgfree: u_int32_t,
}
#[repr(C)]
struct __db_h_stat {
    hash_magic: u_int32_t,
    hash_version: u_int32_t,
    hash_metaflags: u_int32_t,
    hash_nkeys: u_int32_t,
    hash_ndata: u_int32_t,
    hash_pagesize: u_int32_t,
    hash_ffactor: u_int32_t,
    hash_buckets: u_int32_t,
    hash_free: u_int32_t,
    hash_bfree: u_int32_t,
    hash_bigpages: u_int32_t,
    hash_big_bfree: u_int32_t,
    hash_overflows: u_int32_t,
    hash_ovfl_free: u_int32_t,
    hash_dup: u_int32_t,
    hash_dup_free: u_int32_t,
}
#[repr(C)]
struct __db_qam_stat {
    qs_magic: u_int32_t,
    qs_version: u_int32_t,
    qs_metaflags: u_int32_t,
    qs_nkeys: u_int32_t,
    qs_ndata: u_int32_t,
    qs_pagesize: u_int32_t,
    qs_extentsize: u_int32_t,
    qs_pages: u_int32_t,
    qs_re_len: u_int32_t,
    qs_re_pad: u_int32_t,
    qs_pgfree: u_int32_t,
    qs_first_recno: u_int32_t,
    qs_cur_recno: u_int32_t,
}
pub const DB_REGION_MAGIC: u32 = 0x120897;
pub const DB_VERB_DEADLOCK: u32 = 0x0001;
pub const DB_VERB_RECOVERY: u32 = 0x0002;
pub const DB_VERB_REPLICATION: u32 = 0x0004;
pub const DB_VERB_WAITSFOR: u32 = 0x0008;
#[repr(C)]
struct __dblist {
    lh_first: *mut __db,
}
#[repr(C)]
struct __env_links {
    tqe_next: *mut __db_env,
    tqe_prev: *mut *mut __db_env,
}
#[repr(C)]
struct __xa_txn {
    tqh_first: *mut __db_txn,
    tqh_last: *mut *mut __db_txn,
}
pub const DB_TEST_ELECTINIT: u32 = 1;
pub const DB_TEST_ELECTVOTE1: u32 = 2;
pub const DB_TEST_POSTDESTROY: u32 = 3;
pub const DB_TEST_POSTLOG: u32 = 4;
pub const DB_TEST_POSTLOGMETA: u32 = 5;
pub const DB_TEST_POSTOPEN: u32 = 6;
pub const DB_TEST_POSTSYNC: u32 = 7;
pub const DB_TEST_PREDESTROY: u32 = 8;
pub const DB_TEST_PREOPEN: u32 = 9;
pub const DB_TEST_SUBDB_LOCKS: u32 = 10;
pub const DB_ENV_AUTO_COMMIT: u32 = 0x0000001;
pub const DB_ENV_CDB: u32 = 0x0000002;
pub const DB_ENV_CDB_ALLDB: u32 = 0x0000004;
pub const DB_ENV_CREATE: u32 = 0x0000008;
pub const DB_ENV_DBLOCAL: u32 = 0x0000010;
pub const DB_ENV_DIRECT_DB: u32 = 0x0000020;
pub const DB_ENV_DIRECT_LOG: u32 = 0x0000040;
pub const DB_ENV_DSYNC_LOG: u32 = 0x0000080;
pub const DB_ENV_FATAL: u32 = 0x0000100;
pub const DB_ENV_LOCKDOWN: u32 = 0x0000200;
pub const DB_ENV_LOG_AUTOREMOVE: u32 = 0x0000400;
pub const DB_ENV_LOG_INMEMORY: u32 = 0x0000800;
pub const DB_ENV_NOLOCKING: u32 = 0x0001000;
pub const DB_ENV_NOMMAP: u32 = 0x0002000;
pub const DB_ENV_NOPANIC: u32 = 0x0004000;
pub const DB_ENV_OPEN_CALLED: u32 = 0x0008000;
pub const DB_ENV_OVERWRITE: u32 = 0x0010000;
pub const DB_ENV_PRIVATE: u32 = 0x0020000;
pub const DB_ENV_REGION_INIT: u32 = 0x0040000;
pub const DB_ENV_RPCCLIENT: u32 = 0x0080000;
pub const DB_ENV_RPCCLIENT_GIVEN: u32 = 0x0100000;
pub const DB_ENV_SYSTEM_MEM: u32 = 0x0200000;
pub const DB_ENV_THREAD: u32 = 0x0400000;
pub const DB_ENV_TIME_NOTGRANTED: u32 = 0x0800000;
pub const DB_ENV_TXN_NOSYNC: u32 = 0x1000000;
pub const DB_ENV_TXN_WRITE_NOSYNC: u32 = 0x2000000;
pub const DB_ENV_YIELDCPU: u32 = 0x4000000;
#[repr(C)]
pub struct __db_env {
    db_errcall: extern "C" fn(*const DB_ENV, *const char, *const char) -> void,
    db_errfile: *mut FILE,
    db_errpfx: *const char,
    db_msgfile: *mut FILE,
    db_msgcall: extern "C" fn(*const DB_ENV, *const char) -> void,
    db_feedback: extern "C" fn(*mut DB_ENV, int, int) -> void,
    db_paniccall: extern "C" fn(*mut DB_ENV, int) -> void,
    db_malloc: extern "C" fn(size_t) -> *mut void,
    db_realloc: extern "C" fn(*mut void, size_t) -> *mut void,
    db_free: extern "C" fn(*mut void) -> void,
    verbose: u_int32_t,
    app_private: *mut void,
    app_dispatch: extern "C" fn(*mut DB_ENV, *mut DBT, *mut DB_LSN, db_recops) -> int, /* User-specified recovery dispatch. */
    lk_conflicts: *mut u_int8_t,
    lk_modes: int,
    lk_max: u_int32_t,
    lk_max_lockers: u_int32_t,
    lk_max_objects: u_int32_t,
    lk_detect: u_int32_t,
    lk_timeout: db_timeout_t,
    lg_bsize: u_int32_t,
    lg_size: u_int32_t,
    lg_regionmax: u_int32_t,
    mp_gbytes: u_int32_t,
    mp_bytes: u_int32_t,
    mp_ncache: u_int,
    mp_mmapsize: size_t,
    mp_maxopenfd: int,
    mp_maxwrite: int,
    mp_maxwrite_sleep: int,
    rep_eid: int,
    rep_send: extern "C" fn(*mut DB_ENV,
                            *const DBT,
                            *const DBT,
                            *const DB_LSN,
                            int,
                            u_int32_t)
                            -> int,
    tx_max: u_int32_t,
    tx_timestamp: time_t,
    tx_timeout: db_timeout_t,
    db_home: *mut char,
    db_log_dir: *mut char,
    db_tmp_dir: *mut char,
    db_data_dir: *mut *mut char,
    data_cnt: int,
    data_next: int,
    db_mode: int,
    dir_mode: int,
    env_lid: u_int32_t,
    open_flags: u_int32_t,
    reginfo: *mut void,
    lockfhp: *mut DB_FH,
    recover_dtab: *mut extern "C" fn(*mut DB_ENV, *mut DBT, *mut DB_LSN, db_recops, *mut void) -> int, /* Dispatch table for recover funcs. */
    recover_dtab_size: size_t,
    cl_handle: *mut void,
    cl_id: u_int,
    db_ref: int,
    shm_key: long,
    tas_spins: u_int32_t,
    dblist_mutexp: *mut DB_MUTEX,
    dblist: __dblist,
    links: __env_links,
    xa_txn: __xa_txn,
    xa_rmid: int,
    api1_internal: *mut void,
    api2_internal: *mut void,
    passwd: *mut char,
    passwd_len: size_t,
    crypto_handle: *mut void,
    mt_mutexp: *mut DB_MUTEX,
    mti: int,
    mt: *mut u_long,
    close: extern "C" fn(*mut DB_ENV, u_int32_t) -> int,
    dbremove: extern "C" fn(*mut DB_ENV,
                            *mut DB_TXN,
                            *const char,
                            *const char,
                            u_int32_t)
                            -> int,
    dbrename: extern "C" fn(*mut DB_ENV,
                            *mut DB_TXN,
                            *const char,
                            *const char,
                            *const char,
                            u_int32_t)
                            -> int,
    err: extern "C" fn(*const DB_ENV, int, *const char, ...) -> void,
    errx: extern "C" fn(*const DB_ENV, *const char, ...) -> void,
    open: extern "C" fn(*mut DB_ENV, *const char, u_int32_t, int) -> int,
    remove: extern "C" fn(*mut DB_ENV, *const char, u_int32_t) -> int,
    stat_print: extern "C" fn(*mut DB_ENV, u_int32_t) -> int,
    fileid_reset: extern "C" fn(*mut DB_ENV, *mut char, int) -> int,
    is_bigendian: extern "C" fn(void) -> int,
    lsn_reset: extern "C" fn(*mut DB_ENV, *mut char, int) -> int,
    prdbt: extern "C" fn(*mut DBT,
                         int,
                         *const char,
                         *mut void,
                         extern "C" fn(*mut void, *const void) -> int,
                         int)
                         -> int,
    set_alloc: extern "C" fn(*mut DB_ENV,
                             extern "C" fn(size_t) -> *mut void,
                             extern "C" fn(*mut void, size_t) -> *mut void,
                             extern "C" fn(*mut void) -> void)
                             -> int,
    set_app_dispatch: extern "C" fn(*mut DB_ENV,
                                    extern "C" fn(*mut DB_ENV, *mut DBT, *mut DB_LSN, db_recops)
                                                  -> int)
                                    -> int,
    get_data_dirs: extern "C" fn(*mut DB_ENV, *const *mut *mut char) -> int,
    set_data_dir: extern "C" fn(*mut DB_ENV, *const char) -> int,
    get_encrypt_flags: extern "C" fn(*mut DB_ENV, *mut u_int32_t) -> int,
    set_encrypt: extern "C" fn(*mut DB_ENV, *const char, u_int32_t) -> int,
    set_errcall: extern "C" fn(*mut DB_ENV,
                               extern "C" fn(*const DB_ENV, *const char, *const char) -> void)
                               -> void,
    get_errfile: extern "C" fn(*mut DB_ENV, *mut *mut FILE) -> void,
    set_errfile: extern "C" fn(*mut DB_ENV, *mut FILE) -> void,
    get_errpfx: extern "C" fn(*mut DB_ENV, *const *mut char) -> void,
    set_errpfx: extern "C" fn(*mut DB_ENV, *const char) -> void,
    set_feedback: extern "C" fn(*mut DB_ENV, extern "C" fn(*mut DB_ENV, int, int) -> void) -> int,
    get_flags: extern "C" fn(*mut DB_ENV, *mut u_int32_t) -> int,
    set_flags: extern "C" fn(*mut DB_ENV, u_int32_t, int) -> int,
    get_home: extern "C" fn(*mut DB_ENV, *const *mut char) -> int,
    set_intermediate_dir: extern "C" fn(*mut DB_ENV, int, u_int32_t) -> int,
    get_open_flags: extern "C" fn(*mut DB_ENV, *mut u_int32_t) -> int,
    set_paniccall: extern "C" fn(*mut DB_ENV, extern "C" fn(*mut DB_ENV, int) -> void) -> int,
    set_rpc_server: extern "C" fn(*mut DB_ENV, *mut void, *const char, long, long, u_int32_t) -> int,
    get_shm_key: extern "C" fn(*mut DB_ENV, *mut long) -> int,
    set_shm_key: extern "C" fn(*mut DB_ENV, long) -> int,
    set_msgcall: extern "C" fn(*mut DB_ENV,
                               extern "C" fn(*const DB_ENV, *const char) -> void)
                               -> void,
    get_msgfile: extern "C" fn(*mut DB_ENV, *mut *mut FILE) -> void,
    set_msgfile: extern "C" fn(*mut DB_ENV, *mut FILE) -> void,
    get_tas_spins: extern "C" fn(*mut DB_ENV, *mut u_int32_t) -> int,
    set_tas_spins: extern "C" fn(*mut DB_ENV, u_int32_t) -> int,
    get_tmp_dir: extern "C" fn(*mut DB_ENV, *const *mut char) -> int,
    set_tmp_dir: extern "C" fn(*mut DB_ENV, *const char) -> int,
    get_verbose: extern "C" fn(*mut DB_ENV, u_int32_t, *mut int) -> int,
    set_verbose: extern "C" fn(*mut DB_ENV, u_int32_t, int) -> int,
    lg_handle: *mut void,
    get_lg_bsize: extern "C" fn(*mut DB_ENV, *mut u_int32_t) -> int,
    set_lg_bsize: extern "C" fn(*mut DB_ENV, u_int32_t) -> int,
    get_lg_dir: extern "C" fn(*mut DB_ENV, *const *mut char) -> int,
    set_lg_dir: extern "C" fn(*mut DB_ENV, *const char) -> int,
    get_lg_max: extern "C" fn(*mut DB_ENV, *mut u_int32_t) -> int,
    set_lg_max: extern "C" fn(*mut DB_ENV, u_int32_t) -> int,
    get_lg_regionmax: extern "C" fn(*mut DB_ENV, *mut u_int32_t) -> int,
    set_lg_regionmax: extern "C" fn(*mut DB_ENV, u_int32_t) -> int,
    log_archive: extern "C" fn(*mut DB_ENV, *mut *mut *mut char, u_int32_t) -> int,
    log_cursor: extern "C" fn(*mut DB_ENV, *mut *mut DB_LOGC, u_int32_t) -> int,
    log_file: extern "C" fn(*mut DB_ENV, *const DB_LSN, *mut char, size_t) -> int,
    log_flush: extern "C" fn(*mut DB_ENV, *const DB_LSN) -> int,
    log_put: extern "C" fn(*mut DB_ENV, *mut DB_LSN, *const DBT, u_int32_t) -> int,
    log_stat: extern "C" fn(*mut DB_ENV, *mut *mut DB_LOG_STAT, u_int32_t) -> int,
    log_stat_print: extern "C" fn(*mut DB_ENV, u_int32_t) -> int,
    lk_handle: *mut void,
    get_lk_conflicts: extern "C" fn(*mut DB_ENV, *const *mut u_int8_t, *mut int) -> int,
    set_lk_conflicts: extern "C" fn(*mut DB_ENV, *mut u_int8_t, int) -> int,
    get_lk_detect: extern "C" fn(*mut DB_ENV, *mut u_int32_t) -> int,
    set_lk_detect: extern "C" fn(*mut DB_ENV, u_int32_t) -> int,
    set_lk_max: extern "C" fn(*mut DB_ENV, u_int32_t) -> int,
    get_lk_max_locks: extern "C" fn(*mut DB_ENV, *mut u_int32_t) -> int,
    set_lk_max_locks: extern "C" fn(*mut DB_ENV, u_int32_t) -> int,
    get_lk_max_lockers: extern "C" fn(*mut DB_ENV, *mut u_int32_t) -> int,
    set_lk_max_lockers: extern "C" fn(*mut DB_ENV, u_int32_t) -> int,
    get_lk_max_objects: extern "C" fn(*mut DB_ENV, *mut u_int32_t) -> int,
    set_lk_max_objects: extern "C" fn(*mut DB_ENV, u_int32_t) -> int,
    lock_detect: extern "C" fn(*mut DB_ENV, u_int32_t, u_int32_t, *mut int) -> int,
    lock_get: extern "C" fn(*mut DB_ENV,
                            u_int32_t,
                            u_int32_t,
                            *const DBT,
                            db_lockmode_t,
                            *mut DB_LOCK)
                            -> int,
    lock_put: extern "C" fn(*mut DB_ENV, *mut DB_LOCK) -> int,
    lock_id: extern "C" fn(*mut DB_ENV, *mut u_int32_t) -> int,
    lock_id_free: extern "C" fn(*mut DB_ENV, u_int32_t) -> int,
    lock_stat: extern "C" fn(*mut DB_ENV, *mut *mut DB_LOCK_STAT, u_int32_t) -> int,
    lock_stat_print: extern "C" fn(*mut DB_ENV, u_int32_t) -> int,
    lock_vec: extern "C" fn(*mut DB_ENV,
                            u_int32_t,
                            u_int32_t,
                            *mut DB_LOCKREQ,
                            int,
                            *mut *mut DB_LOCKREQ)
                            -> int,
    mp_handle: *mut void,
    get_cachesize: extern "C" fn(*mut DB_ENV, *mut u_int32_t, *mut u_int32_t, *mut int) -> int,
    set_cachesize: extern "C" fn(*mut DB_ENV, u_int32_t, u_int32_t, int) -> int,
    get_mp_mmapsize: extern "C" fn(*mut DB_ENV, *mut size_t) -> int,
    set_mp_mmapsize: extern "C" fn(*mut DB_ENV, size_t) -> int,
    get_mp_max_openfd: extern "C" fn(*mut DB_ENV, *mut int) -> int,
    set_mp_max_openfd: extern "C" fn(*mut DB_ENV, int) -> int,
    get_mp_max_write: extern "C" fn(*mut DB_ENV, *mut int, *mut int) -> int,
    set_mp_max_write: extern "C" fn(*mut DB_ENV, int, int) -> int,
    memp_fcreate: extern "C" fn(*mut DB_ENV, *mut *mut DB_MPOOLFILE, u_int32_t) -> int,
    memp_register: extern "C" fn(*mut DB_ENV,
                                 int,
                                 extern "C" fn(*mut DB_ENV, db_pgno_t, *mut void, *mut DBT) -> int,
                                 extern "C" fn(*mut DB_ENV, db_pgno_t, *mut void, *mut DBT) -> int)
                                 -> int,
    memp_stat: extern "C" fn(*mut DB_ENV,
                             *mut *mut DB_MPOOL_STAT,
                             *mut *mut *mut DB_MPOOL_FSTAT,
                             u_int32_t)
                             -> int,
    memp_stat_print: extern "C" fn(*mut DB_ENV, u_int32_t) -> int,
    memp_sync: extern "C" fn(*mut DB_ENV, *mut DB_LSN) -> int,
    memp_trickle: extern "C" fn(*mut DB_ENV, int, *mut int) -> int,
    rep_handle: *mut void,
    rep_elect: extern "C" fn(*mut DB_ENV, int, int, int, u_int32_t, *mut int, u_int32_t) -> int,
    rep_flush: extern "C" fn(*mut DB_ENV) -> int,
    rep_process_message: extern "C" fn(*mut DB_ENV, *mut DBT, *mut DBT, *mut int, *mut DB_LSN) -> int,
    rep_start: extern "C" fn(*mut DB_ENV, *mut DBT, u_int32_t) -> int,
    rep_stat: extern "C" fn(*mut DB_ENV, *mut *mut DB_REP_STAT, u_int32_t) -> int,
    rep_stat_print: extern "C" fn(*mut DB_ENV, u_int32_t) -> int,
    get_rep_limit: extern "C" fn(*mut DB_ENV, *mut u_int32_t, *mut u_int32_t) -> int,
    set_rep_limit: extern "C" fn(*mut DB_ENV, u_int32_t, u_int32_t) -> int,
    set_rep_request: extern "C" fn(*mut DB_ENV, u_int32_t, u_int32_t) -> int,
    set_rep_transport: extern "C" fn(*mut DB_ENV,
                                     int,
                                     extern "C" fn(*mut DB_ENV,
                                                   *const DBT,
                                                   *const DBT,
                                                   *const DB_LSN,
                                                   int,
                                                   u_int32_t)
                                                   -> int)
                                     -> int,
    tx_handle: *mut void,
    get_tx_max: extern "C" fn(*mut DB_ENV, *mut u_int32_t) -> int,
    set_tx_max: extern "C" fn(*mut DB_ENV, u_int32_t) -> int,
    get_tx_timestamp: extern "C" fn(*mut DB_ENV, *mut time_t) -> int,
    set_tx_timestamp: extern "C" fn(*mut DB_ENV, *mut time_t) -> int,
    txn_begin: extern "C" fn(*mut DB_ENV, *mut DB_TXN, *mut *mut DB_TXN, u_int32_t) -> int,
    txn_checkpoint: extern "C" fn(*mut DB_ENV, u_int32_t, u_int32_t, u_int32_t) -> int,
    txn_recover: extern "C" fn(*mut DB_ENV, *mut DB_PREPLIST, long, *mut long, u_int32_t) -> int,
    txn_stat: extern "C" fn(*mut DB_ENV, *mut *mut DB_TXN_STAT, u_int32_t) -> int,
    txn_stat_print: extern "C" fn(*mut DB_ENV, u_int32_t) -> int,
    get_timeout: extern "C" fn(*mut DB_ENV, *mut db_timeout_t, u_int32_t) -> int,
    set_timeout: extern "C" fn(*mut DB_ENV, db_timeout_t, u_int32_t) -> int,
    test_abort: int,
    test_check: int,
    test_copy: int,
    flags: u_int32_t,
}
pub const DB_DBM_HSEARCH: u32 = 0;
pub type DBM = __db;
pub const DBM_INSERT: u32 = 0;
pub const DBM_REPLACE: u32 = 1;
pub const DBM_SUFFIX: &'static str = ".db";
#[repr(C)]
pub struct datum {
    dptr: *mut char,
    dsize: int,
}
macro_rules! dbm_clearerr { (a) => (__db_ndbm_clearerr(a)) }
macro_rules! dbm_close { (a) => (__db_ndbm_close(a)) }
macro_rules! dbm_delete { (a, b) => (__db_ndbm_delete(a, b)) }
macro_rules! dbm_dirfno { (a) => (__db_ndbm_dirfno(a)) }
macro_rules! dbm_error { (a) => (__db_ndbm_error(a)) }
macro_rules! dbm_fetch { (a, b) => (__db_ndbm_fetch(a, b)) }
macro_rules! dbm_firstkey { (a) => (__db_ndbm_firstkey(a)) }
macro_rules! dbm_nextkey { (a) => (__db_ndbm_nextkey(a)) }
macro_rules! dbm_open { (a, b, c) => (__db_ndbm_open(a, b, c)) }
macro_rules! dbm_pagfno { (a) => (__db_ndbm_pagfno(a)) }
macro_rules! dbm_rdonly { (a) => (__db_ndbm_rdonly(a)) }
macro_rules! dbm_store { (a, b, c, d) => (__db_ndbm_store(a, b, c, d)) }
macro_rules! dbminit { (a) => (__db_dbm_init(a)) }
macro_rules! dbmclose { () => (__db_dbm_close()) }
macro_rules! delete { (a) => (__db_dbm_delete(a)) }
macro_rules! fetch { (a) => (__db_dbm_fetch(a)) }
macro_rules! firstkey { () => (__db_dbm_firstkey()) }
macro_rules! nextkey { (a) => (__db_dbm_nextkey(a)) }
macro_rules! store { (a, b) => (__db_dbm_store(a, b)) }
#[repr(C)]
pub enum ACTION {
    FIND,
    ENTER,
}
#[repr(C)]
pub struct entry {
    key: *mut char,
    data: *mut char,
}
pub type ENTRY = entry;
macro_rules! hcreate { (a) => (__db_hcreate(a)) }
macro_rules! hdestroy { () => (__db_hdestroy()) }
macro_rules! hsearch { (a, b) => (__db_hsearch(a, b)) }
extern "C" {
    pub fn db_create(dbp: *mut *mut DB, dbenv: *mut DB_ENV, flags: u_int32_t) -> int;
    pub fn db_strerror(error: int) -> *mut char;
    pub fn db_env_create(dbenvp: *mut *mut DB_ENV, flags: u_int32_t) -> int;
    pub fn db_version(major: *mut int, minor: *mut int, patch: *mut int) -> *mut char;
    pub fn log_compare(lsn0: *const DB_LSN, lsn1: *const DB_LSN) -> int;
    pub fn db_env_set_func_close(f: extern "C" fn(int) -> int) -> int;
    pub fn db_env_set_func_dirfree(f: extern "C" fn(*mut *mut char, int) -> void) -> int;
    pub fn db_env_set_func_dirlist(f: extern "C" fn(*const char, *mut *mut *mut char, *mut int)
                                                    -> int)
                                   -> int;
    pub fn db_env_set_func_exists(f: extern "C" fn(*const char, *mut int) -> int) -> int;
    pub fn db_env_set_func_free(f: extern "C" fn(*mut void) -> void) -> int;
    pub fn db_env_set_func_fsync(f: extern "C" fn(int) -> int) -> int;
    pub fn db_env_set_func_ftruncate(f: extern "C" fn(int, off_t) -> int) -> int;
    pub fn db_env_set_func_ioinfo(f: extern "C" fn(*const char,
                                                   int,
                                                   *mut u_int32_t,
                                                   *mut u_int32_t,
                                                   *mut u_int32_t)
                                                   -> int)
                                  -> int;
    pub fn db_env_set_func_malloc(f: extern "C" fn(size_t) -> *mut void) -> int;
    pub fn db_env_set_func_map(f: extern "C" fn(*mut char, size_t, int, int, *mut *mut void) -> int)
                               -> int;
    pub fn db_env_set_func_pread(f: extern "C" fn(int, *mut void, size_t, off_t) -> ssize_t) -> int;
    pub fn db_env_set_func_pwrite(f: extern "C" fn(int, *const void, size_t, off_t) -> ssize_t)
                                  -> int;
    pub fn db_env_set_func_open(f: extern "C" fn(*const char, int, ...) -> int) -> int;
    pub fn db_env_set_func_read(f: extern "C" fn(int, *mut void, size_t) -> ssize_t) -> int;
    pub fn db_env_set_func_realloc(f: extern "C" fn(*mut void, size_t) -> *mut void) -> int;
    pub fn db_env_set_func_rename(f: extern "C" fn(*const char, *const char) -> int) -> int;
    pub fn db_env_set_func_seek(f: extern "C" fn(int, off_t, int) -> int) -> int;
    pub fn db_env_set_func_sleep(f: extern "C" fn(u_long, u_long) -> int) -> int;
    pub fn db_env_set_func_unlink(f: extern "C" fn(*const char) -> int) -> int;
    pub fn db_env_set_func_unmap(f: extern "C" fn(*mut void, size_t) -> int) -> int;
    pub fn db_env_set_func_write(f: extern "C" fn(int, *const void, size_t) -> ssize_t) -> int;
    pub fn db_env_set_func_yield(f: extern "C" fn(void) -> int) -> int;
    pub fn db_sequence_create(seq: *mut *mut DB_SEQUENCE, db: *mut DB, flags: u_int32_t) -> int;
    pub fn __db_ndbm_clearerr(db: *mut DBM) -> int;
    pub fn __db_ndbm_close(db: *mut DBM) -> void;
    pub fn __db_ndbm_delete(db: *mut DBM, key: datum) -> int;
    pub fn __db_ndbm_dirfno(db: *mut DBM) -> int;
    pub fn __db_ndbm_error(db: *mut DBM) -> int;
    pub fn __db_ndbm_fetch(db: *mut DBM, key: datum) -> datum;
    pub fn __db_ndbm_firstkey(db: *mut DBM) -> datum;
    pub fn __db_ndbm_nextkey(db: *mut DBM) -> datum;
    pub fn __db_ndbm_open(file: *const char, flags: int, mode: int) -> *mut DBM;
    pub fn __db_ndbm_pagfno(db: *mut DBM) -> int;
    pub fn __db_ndbm_rdonly(db: *mut DBM) -> int;
    pub fn __db_ndbm_store(db: *mut DBM, key: datum, content: datum, flags: int) -> int;
    pub fn __db_dbm_close() -> int;
    pub fn __db_dbm_delete(key: datum) -> int;
    pub fn __db_dbm_fetch(key: datum) -> datum;
    pub fn __db_dbm_firstkey() -> datum;
    pub fn __db_dbm_init(file: *mut char) -> int;
    pub fn __db_dbm_nextkey(key: datum) -> datum;
    pub fn __db_dbm_store(key: datum, content: datum) -> int;
    pub fn __db_hcreate(nelem: size_t) -> int;
    pub fn __db_hsearch(item: ENTRY, action: ACTION) -> *mut ENTRY;
    pub fn __db_hdestroy();
}
